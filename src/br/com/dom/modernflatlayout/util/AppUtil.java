package br.com.dom.modernflatlayout.util;


import br.com.dom.modernflatlayout.controller.RootController;
import br.com.dom.modernflatlayout.view.animations.ShakeTransition;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

/**
 * Created by DOM on 27/10/2016.
 */
public class AppUtil {

    public static final String ROOT_PATH = "br/com/dom/labestoquefx";

    public static boolean fieldInputNotNull(final TextField field) {
        registerTypeEvent(field);
        if (field.getText().length() <= 0) {
            field.setStyle("-fx-border-color:red;");
            new ShakeTransition(field).play();
            return false;
        } else {
            return true;
        }
    }

    public static void TextFieldToUpperCase(final TextField txt) {
        txt.textProperty().addListener((observableValue, s, t1) -> txt.setText(t1.toUpperCase()));
    }

    private static void registerTypeEvent(final TextField field) {
        field.lengthProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() > 0) {
                field.setStyle(null);
            }
        });
    }

    public static Image getImage(String imageName) {
        String img = imageName.endsWith(".png") ? imageName : imageName.concat(".png");
        return new Image("resources/img/"+img);
    }


    public static void showStageOwner(Stage stage, StageStyle stageStyle, Parent parent, EventHandler<WindowEvent> eventHandler) {
        stage.close();

        stage.setScene(new Scene(parent));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initStyle(stageStyle != null ? stageStyle : StageStyle.UTILITY);
        RootController.root.getRegionOverlayEffect()
                .visibleProperty().bind(stage.showingProperty());
        stage.initOwner(RootController.root.getRegionOverlayEffect().getScene().getWindow());
        stage.show();
        stage.setResizable(false);
        stage.setOnCloseRequest(eventHandler);
    }

}
