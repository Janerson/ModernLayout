package br.com.dom.modernflatlayout.controller;


import br.com.dom.modernflatlayout.util.AppUtil;
import br.com.dom.modernflatlayout.util.FXMLUtil;
import br.com.dom.modernflatlayout.view.animations.FadeInRightTransition;
import br.com.dom.modernflatlayout.view.java.Dialog;
import eu.hansolo.enzo.roundlcdclock.RoundLcdClock;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.BoundingBox;
import javafx.geometry.Rectangle2D;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by DOM on 26/03/2017.
 * email: douglas.janerson@gmail.com
 */
public class RootController implements Initializable {

    public static RootController root;

    @FXML
    private RoundLcdClock clock;

    @FXML
    private AnchorPane contentPane;

    @FXML
    private AnchorPane base;

    @FXML
    private HBox topBar;

    @FXML
    private Region veil, region;
    @FXML
    private Text txtTitle;

    double positionX, positionY;
    private Stage st;
    private boolean maximized;
    private BoundingBox savedBounds;
    //private Stage stage;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        init();
    }

    private void init() {

        root = this;
        clock.setColor(Color.WHITE);
        clock.setOpacity(0.6);

        contentPane.prefWidthProperty().bind(base.widthProperty());
        contentPane.prefHeightProperty().bind(base.heightProperty());
        region.prefWidthProperty().bind(base.widthProperty());


        topBar.setOnMousePressed(m -> {
            positionX = m.getSceneX();
            positionY = m.getSceneY();
        });

        topBar.setOnMouseDragged(m -> {
            topBar.getScene().getWindow().setX(m.getScreenX() - positionX);
            topBar.getScene().getWindow().setY(m.getScreenY() - positionY);
        });
        showLogin();
    }

    private void showLogin(){
        Platform.runLater(() -> AppUtil.showStageOwner(new Stage(), StageStyle.UNDECORATED,
                FXMLUtil.getParent("app_login"), null));
    }

    @FXML
    private void actionButton(Event event) {
        String id = ((HBox) event.getSource()).getId();
        contentPane.setOpacity(0);
    //    contentPane.getChildren().remove(0);
        txtTitle.setOpacity(0);
        switch (id) {
            case "home":
                System.out.println("HOME");
                /*Exemplo de uso da classe br.com.dom.modernflatlayout.view.java.Dialog*/
                new Dialog(Dialog.DialogType.ERROR,"HOME", "View não implementada").show();
                break;
            case "saida":
                System.out.println("SAIDA");
                new Dialog(Dialog.DialogType.WARNING,"SAIDA", "View não implementada").show();
                break;
            case "entrada":
                System.out.println("ENTRADA");
                new Dialog(Dialog.DialogType.INFO,"ENTRADA", "View não implementada").show();
                break;
            case "charts":
                System.out.println("CHARTS");
                new Dialog(Dialog.DialogType.SUCCESS,"CHARTS", "View não implementada").show();
                break;
            case "produtos":
                System.out.println("PRODUTOS");
                /*Exemplo de uso para fazer a transicao das views*/
                changeView("app_profile","Perfil");
                break;
            case "logout":
                System.out.println("LOGOUT");
                showLogin();
                break;
        }
    }

    /**
     *
     * @param fxmlName Nome do arquivo fxml
     * @param title Titulo idicativo da view que irá aparecer na barra de titulo
     */
    private void changeView(String fxmlName, String title) {
        contentPane.getChildren().add(0,FXMLUtil.getParent(fxmlName));
        txtTitle.setText(title);
        new FadeInRightTransition(contentPane).play();
        new FadeInRightTransition(txtTitle).play();
        ((AnchorPane)contentPane.getChildren().get(0)).prefWidthProperty().bind(contentPane.widthProperty());
        ((AnchorPane)contentPane.getChildren().get(0)).prefHeightProperty().bind(contentPane.heightProperty());

    }

    @FXML
    void close() {
        System.exit(0);
    }

    @FXML
    void expand() {
        st = (Stage) base.getScene().getWindow();
        if (maximized) {
            restoreSavedBounds(st);
            savedBounds = null;
            maximized = false;
        } else {
            ObservableList<Screen> screensForRectangle = Screen.getScreensForRectangle(st.getX(), st.getY(), st.getWidth(), st.getHeight());
            Screen screen = screensForRectangle.get(0);
            Rectangle2D visualBounds = screen.getVisualBounds();

            savedBounds = new BoundingBox(st.getX(), st.getY(), st.getWidth(), st.getHeight());

            st.setX(visualBounds.getMinX());
            st.setY(visualBounds.getMinY());
            st.setWidth(visualBounds.getWidth());
            st.setHeight(visualBounds.getHeight());
            maximized = true;
        }
    }

    private void restoreSavedBounds(Stage stage) {
        stage.setX(savedBounds.getMinX());
        stage.setY(savedBounds.getMinY());
        stage.setWidth(savedBounds.getWidth());
        stage.setHeight(savedBounds.getHeight());
        savedBounds = null;
    }

    @FXML
    void minimize() {
        ((Stage) clock.getParent().getScene().getWindow()).setIconified(true);
    }

    public Region getRegionOverlayEffect() {
        return veil;
    }

}
