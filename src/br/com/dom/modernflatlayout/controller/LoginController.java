package br.com.dom.modernflatlayout.controller;

import br.com.dom.modernflatlayout.util.AppUtil;
import br.com.dom.modernflatlayout.view.animations.ShakeTransition;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.controlsfx.control.textfield.CustomPasswordField;
import org.controlsfx.control.textfield.CustomTextField;
import org.kordamp.ikonli.fontawesome.FontAwesome;
import org.kordamp.ikonli.javafx.FontIcon;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by DOM on 28/03/2017.
 * email: douglas.janerson@gmail.com
 */
public class LoginController implements Initializable {

    @FXML
    private CustomTextField ctfLogin;
    @FXML
    private CustomPasswordField ctfPass;
    @FXML
    private Button btnLogin;
    @FXML
    private AnchorPane loginAnchor;
    @FXML
    private Label lblMessage;
    @FXML
    private ImageView alertImageView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        init();
    }

    void init() {

        ctfLogin.setLeft(new FontIcon(FontAwesome.USER));
        ctfPass.setLeft(new FontIcon(FontAwesome.LOCK));

        ctfPass.setOnKeyPressed(k -> {
            if (k.getCode() == KeyCode.ENTER |
                    k.getCode() == KeyCode.TAB) {

                if (validateField()) {
                    String user = ctfLogin.getText();
                    String pass = ctfPass.getText();
                    login(user, pass);
                }
            }
        });

        btnLogin.setOnAction(a -> {
            if (validateField()) {
                String user = ctfLogin.getText();
                String pass = ctfPass.getText();
                login(user, pass);
            }
        });

    }


    private void login(String user, String pass) {


        if (user.equals("admin") && pass.equals("admin")) {
            ((Stage) btnLogin.getScene().getWindow()).close();

        } else {
            new ShakeTransition(loginAnchor).play();
            showLoginErr();
        }

    }

    private void showLoginErr() {
        lblMessage.setVisible(true);
    }

    private boolean validateField() {

        return AppUtil.fieldInputNotNull(ctfLogin) &
                AppUtil.fieldInputNotNull(ctfPass);
    }
}
