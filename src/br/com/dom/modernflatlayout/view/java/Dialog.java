package br.com.dom.modernflatlayout.view.java;


import br.com.dom.modernflatlayout.util.AppUtil;
import br.com.dom.modernflatlayout.util.FXMLUtil;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

/**
 * Created by LAB on 28/04/2017.
 */
public class Dialog {

    @FXML
    private VBox vbox;
    @FXML
    private ImageView icon;
    @FXML
    private Button btnOK;
    @FXML
    private Text title, message;

    private DialogType type;

    private FXMLLoader loader;

    private AnchorPane root;
    private Stage t;

    /**
     * @param dialogType Tipo de mensagem [error , warning, info,success]
     * @param title
     * @param message
     */
    public Dialog(DialogType dialogType, String title, String message) {
        this.type = dialogType;

        try {
            loader = new FXMLLoader(FXMLUtil.getFXML("app_dialog"));
            loader.setController(this);
            root = loader.load();
            btnOK.setOnAction(a -> t.close());
            this.title.setText(title);
            this.message.setText(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void show() {
        Scene scene = new Scene(root);
        t = new Stage();
        t.initStyle(StageStyle.UNDECORATED);
        t.setScene(scene);
        updateStyleClass();
        t.show();
    }

    private void updateStyleClass() {
        root.getStyleClass().remove(0);
        root.getStyleClass().add("base-" + type);
        vbox.getStyleClass().remove(0);
        vbox.getStyleClass().add("dialog-" + type);
        btnOK.getStyleClass().remove(0);
        btnOK.getStyleClass().add("btn-" + type);
        icon.setImage(AppUtil.getImage(""+type));
    }

    public enum DialogType {
        ERROR("error"),
        WARNING("warning"),
        INFO("info"),
        SUCCESS("success");
        private String type;

        DialogType(String dialogType) {
            type = dialogType;
        }

        @Override
        public String toString() {
            return type;
        }
    }


}
